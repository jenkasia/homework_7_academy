import React from 'react';
import Footer from './components/Footer';
import Header from './components/Header';
import Chat from './components/Chat';
import './App.sass';

const App: React.FC = () => {
  return (
    <>
      <Header />
      <Chat />
      <Footer />
    </>
  );
};

export default App;

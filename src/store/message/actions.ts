import { ADD_MESSAGE, REMOVE_MESSAGE, LIKE_MESSAGE, EDIT_MESSAGE, GET_MESSAGES, HIDE_LOADER } from '../types';
import IMessage from '../../Interfaces/IMessage';

export const addMessage = (message: IMessage) => {
  return {
    type: ADD_MESSAGE,
    payload: message
  };
};

export const removeMessage = (messageID: string) => {
  return {
    type: REMOVE_MESSAGE,
    payload: messageID
  };
};

export const editMessage = (messageID: string, newMessage: string) => {
  return {
    type: EDIT_MESSAGE,
    payload: { messageID, newMessage }
  };
};

export const likeDislikeMessage = (messageID: string) => {
  return {
    type: LIKE_MESSAGE,
    payload: messageID
  };
};

export const getMessages = () => {
  return async (dispatch: Function) => {
    const startDate = Date.now();
    const response = await fetch(
      'https://gist.githubusercontent.com/jenkasia/25a488a103525a8278c9e7695319c9d0/raw/91d5ed1fdd700aa2f15cf70248f44df1cc4786c5/messages'
    );
    const json = await response.json();
    const endDate = Date.now();
    setTimeout(() => {
      dispatch({ type: GET_MESSAGES, payload: json });
      dispatch({ type: HIDE_LOADER });
    }, 1200 * Math.ceil((endDate - startDate) / 1200));
  };
};

import { ADD_MESSAGE, REMOVE_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, GET_MESSAGES } from '../types';
import IMessage from '../../Interfaces/IMessage';

type Action = {
  type: string;
  payload: any;
};

type State = {
  likedMessages: Array<string>;
  messages: Array<IMessage>;
};

const initialState: State = {
  likedMessages: [],
  messages: []
};

export const messageReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case ADD_MESSAGE: {
      return { ...state, messages: state.messages.concat(action.payload) };
    }
    case EDIT_MESSAGE: {
      let messageIndex = state.messages.findIndex((message) => action.payload.messageID === message.id);
      const updatedMessage = state.messages[messageIndex];
      updatedMessage!.editedAt! = new Date().toString();
      updatedMessage!.text = action.payload.newMessage;
      const newMessageArray = state.messages
        .slice(0, messageIndex)
        .concat(updatedMessage, state.messages.slice(messageIndex + 1, state.messages.length));
      return { ...state, messages: newMessageArray };
    }
    case REMOVE_MESSAGE: {
      const messageIndex = state.messages.findIndex((message) => action.payload === message.id);
      const newMessageArray = state.messages
        .slice(0, messageIndex)
        .concat(state.messages.slice(messageIndex + 1, state.messages.length));
      return { ...state, messages: newMessageArray };
    }
    case LIKE_MESSAGE: {
      let uniqueLikes: Set<string> = new Set(state.likedMessages);
      if (uniqueLikes.has(action.payload)) {
        uniqueLikes.delete(action.payload);
      } else {
        uniqueLikes.add(action.payload);
      }
      const newArray = Array.from(uniqueLikes);
      return { ...state, likedMessages: newArray };
    }
    case GET_MESSAGES: {
      return { ...state, messages: action.payload };
    }
    default: {
      return { ...state };
    }
  }
};

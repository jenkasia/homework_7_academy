import { combineReducers } from 'redux';
import { appReducer } from './chatInfo/reducer';
import { messageReducer } from './message/reducer';

export default combineReducers({
  app: appReducer,
  messages: messageReducer
});

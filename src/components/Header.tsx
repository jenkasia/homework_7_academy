import React from 'react';
import Wrapper from './Wrapper';
import logo from '../images/logo.png';
import './basic.sass';

const Header: React.FC = () => {
  return (
    <header className='header'>
      <Wrapper class='header__block'>
        <img className='header__logo' src={logo} alt='logo' />
        <p>Binary studio academy</p>
      </Wrapper>
    </header>
  );
};

export default Header;

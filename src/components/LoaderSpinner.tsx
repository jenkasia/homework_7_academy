import React from 'react';
import './spinner.sass';

const LoaderSpinner: React.FC = () => {
  return (
    <div className='spinner-wrapper'>
      <div className='spinner'></div>
    </div>
  );
};

export default LoaderSpinner;

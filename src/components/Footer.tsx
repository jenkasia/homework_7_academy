import React from 'react';
import './basic.sass';

const Footer: React.FC = () => {
  return (
    <footer className='footer'>
      <span className='footer__icon'>&#169;</span>Copyrighting
    </footer>
  );
};

export default Footer;

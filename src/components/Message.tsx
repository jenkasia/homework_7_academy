import React from 'react';
import like from '../images/love-icon_fill.svg';
import dislike from '../images/love-icon.svg';
import remove from '../images/trash.svg';
import edit from '../images/edit_icon.svg';

type MessageProps = {
  messageType: 'personal' | '';
  messageText: string;
  imageSrc: string;
  userName: string;
  messageID: string;
  editableMessage: boolean;
  messageData: string;
  likedMessagesList: Array<string>;
  onRemoveMessage(messageID: string): void;
  onLikeDislikeMessage(messageID: string): void;
  onEditMessage(messageID: string, messageText: string): void;
};

const Message: React.FC<MessageProps> = (props) => {
  const onRemoveHandler = (event: React.MouseEvent) => {
    const shouldRemove = window.confirm('Вы уверены, что хотите удалить это сообщение?');
    if (shouldRemove) {
      props.onRemoveMessage(props.messageID);
    }
  };

  const onEditHandler = (event: React.MouseEvent) => {
    props.onEditMessage(props.messageID, props.messageText);
  };

  const onLikeDislikeHandler = (event: React.MouseEvent) => {
    props.onLikeDislikeMessage(props.messageID);
  };

  const isMessageLike = () => {
    return props.likedMessagesList.includes(props.messageID);
  };

  return props.messageType ? (
    <div className='message message_personal'>
      <p className='message__text'>{props.messageText}</p>
      <span className='message__date'>{props.messageData}</span>
      <div className='message__remove-box message__controller-box' onClick={onRemoveHandler}>
        <img className='message__remove-icon message__controller-icon' src={remove} alt='remove' />
      </div>
      {props.editableMessage && (
        <div className='message__edit-box  message__controller-box' onClick={onEditHandler}>
          <img className='message__edit-icon  message__controller-icon' src={edit} alt='remove' />
        </div>
      )}
    </div>
  ) : (
    <div className='message'>
      <img src={props.imageSrc} alt='avatar' className='message__img' />
      <p className='message__userName'>{props.userName}</p>
      <p className='message__text'>{props.messageText}</p>
      <img src={isMessageLike() ? like : dislike} alt='' className='message__like' onClick={onLikeDislikeHandler} />
      <span className='message__date'>{props.messageData}</span>
    </div>
  );
};

export default Message;
